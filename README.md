# Binary grpc-tools build for M1
grpc-node maintainers chose not to provide arm64 builds of grpc-tools. As a result, any node projects that depend on `grpc-tools` (or `grpc`/`grpc-js` that depend on `grpc-tools`) will fail on darwin-arm64 native. 

See upstream [issue](https://github.com/grpc/grpc-node/issues/1405).

While the upstream is figuring out the resolution to this problem this repo can be used as workaround.

## Using this mirror

Set  `npm_config_grpc_tools_binary_host_mirror` environment variable to point to this repo. 

Example:
```
npm_config_grpc_tools_binary_host_mirror="https://gitlab.com/gromodanovskiy/grpc-tools-m1/-/raw/main/artifacts" yarn install
```

## Building grpc-tools binary packages
 - Clone the upstream [grpc-node](https://github.com/grpc/grpc-node)
 - Checkout the desired version (`git checkout grpc-tools@1.x.x`)
 - Update submodules (`git submodule update --init --recursive`)
 - Build the binaries (`cd packages/grpc-tools && ./build_binaries.sh`)
 - Archive the binaries (`cd build/ && tar -czvf darwin-arm64.tar.gz bin/`)